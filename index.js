const base = require('base.apicloak.plugin');
const settings_schema = require('./settings.schema.js');
const async = require('async');

module.exports = function () {
    return Object.assign({}, base('transaction_persistent_postgres', settings_schema), {
        executeAction: function (data, callback) {
            console.log('data: ', data)
            if (data.creation_date) {
                console.log('persistent postgres plugin settings: ', this.settings);
                let pgp = require('pg-promise')();
                let db = pgp('postgres://' + this.settings.username + ':' + this.settings.password + '@' + this.settings.host + ':' + this.settings.port + '/' + this.settings.db_name);



                let init_data = Object.assign({}, data, {settings: this.settings});
                async.waterfall([
                    function setInitDataStep(done) {
                        done(null,init_data)
                    },
                    function createTransactionsTableIfNotExistsStep(data,done) {

                        let create_sequence_query = `CREATE SEQUENCE IF NOT EXISTS ` + data.settings.transactions_table + '_id_seq';

                        let create_table_query = `CREATE TABLE IF NOT EXISTS ${data.settings.transactions_table} (
                                                    id INTEGER NOT NULL DEFAULT nextval('${data.settings.transactions_table + '_id_seq'}'),
                                                    creation_date TIMESTAMP NOT NULL,
                                                    consumer INTEGER NOT NULL,
                                                    provider INTEGER NOT NULL,
                                                    CONSTRAINT ${data.settings.transactions_table + '_id'} PRIMARY KEY (id)
                                                  )`;

                        db.none({
                            text: create_sequence_query
                        }).then(function (create_sequence_result) {
                            db.none({
                                text: create_table_query
                            }).then(function (create_table_result) {
                                console.log('create table result: ', create_table_result);
                                console.log('data in prmise: ', data);
                                done(null, data);
                            }).catch(function (error) {
                                console.log('error: ', error);
                                done(error);
                            });
                        }).catch(function (error) {
                            console.log('error: ', error);
                            done(error);
                        });

                    },
                    function saveTransactionStep(data, done) {
                        let insert_query = `INSERT INTO ${data.settings.transactions_table}(creation_date,consumer,provider) VALUES($1,$2,$3)`;
                        console.log('data before insert: ', data)
                        db.none({
                            text: insert_query,
                            values: [data.creation_date, data.consumer.id, data.provider.id]
                        }).then(function (result) {
                            console.log('insert transaction result: ', result);

                            done(null, data);
                        }).catch(function (error) {
                            console.log('error: ', error);
                            done(error);
                        });
                    }
                ], function (err, result) {
                    console.log('Saving transaction finished. Err: ', err, ' result: ', result)
                    callback(err, result);
                });

            } else {
                callback('Data is missing transaction creation date');
            }
        }
    });
};
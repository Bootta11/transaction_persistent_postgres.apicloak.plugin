const chai = require('chai');
const expect = chai.expect;
const should = chai.should();

describe('Transaction persistent postgres plugin', function () {
    let plugin = {};
    before(function (done) {
        plugin = require('./index.js')();
        done();
    });

    describe('action() method', function () {
        let result = {};
        before(function (done) {
            plugin.load({
                username: 'postgres',
                password: 'test1234',
                host: '127.0.0.1',
                port: '5432',
                db_name: 'proxy_api',
                transactions_table: 'transaction_temp'
            });
            let date = new Date();
            let date_part = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            let time_part = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds();

            plugin.action({creation_date: date_part + ' ' + time_part, consumer: {id: 0}, provider: {id: 3}}, function (err, data) {
                result.data = data;
                result.error = err;

                done();
            });
        });
        it('should return no error', function () {
            expect(result.error).to.be.null;
        });
        it('should return data object', function () {
            expect(result.data).is.an('object');
        });
    });


});
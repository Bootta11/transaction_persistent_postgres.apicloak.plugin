const schema = {
    properties: {
        username: {type: 'string', default: 'db_username'},
        password: {type: 'string', default: 'db_password'},
        host: {type: 'string', default: 'db_host'},
        port: {type: 'string', default: 'db_port'},
        db_name: {type: 'string', default: 'db_name'},
        transactions_table: {type: 'string', default: 'transactions_table_name'}
    },
    required: ['username','password','host','port','db_name','transactions_table']
};

module.exports = schema;